#\!/bin/bash
# Simple Jansible Application Installer

SETUP_DIR="$HOME/.setups"

echo "=== Jansible Application Installer ==="
echo "The following applications are available:"
echo

# List applications with status
for APP_DIR in "$SETUP_DIR"/*; do
  if [ -d "$APP_DIR" ] && [ -f "$APP_DIR/install.sh" ]; then
    APP_NAME=$(basename "$APP_DIR")
    if [ "$APP_NAME" \!= "run_installers.sh" ]; then
      if [ -f "$APP_DIR/installed" ]; then
        INSTALL_DATE=$(cat "$APP_DIR/installed")
        echo "  [✓] $APP_NAME (installed on $INSTALL_DATE)"
      else
        echo "  [ ] $APP_NAME (not installed)"
      fi
    fi
  fi
done

echo
echo "To install an app, run its install script:"
echo "  ~/.setups/<app-name>/install.sh"
echo
echo "For example: ~/.setups/discord/install.sh"
