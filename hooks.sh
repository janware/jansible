#!/bin/bash

mkdir -p .git/hooks
cp hooks/pre-commit .git/hooks/pre-commit
cp hooks/post-checkout .git/hooks/post-checkout
chmod +x .git/hooks/pre-commit .git/hooks/post-checkout