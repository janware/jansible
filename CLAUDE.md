# Jansible Project Guidelines

## Commands
- Run playbook: `ansible-playbook playbooks/<path>/<playbook>.yml`
- Run against specific hosts: `ansible-playbook -l <hostname> playbooks/<path>/<playbook>.yml`
- Run specific tasks: `ansible-playbook playbooks/<path>/<playbook>.yml --tags <tag>`
- Skip tags: `ansible-playbook playbooks/<path>/<playbook>.yml --skip-tags <tag>`
- Check mode: `ansible-playbook --check playbooks/<path>/<playbook>.yml`
- Syntax check: `ansible-playbook --syntax-check playbooks/<path>/<playbook>.yml`

## Style Guidelines
- Use `ansible.builtin` prefix for all Ansible modules
- Task naming: "feature | topic | description" (e.g., "users | ansible | ensure packages installed")
- Use lowercase in comments unless proper nouns are needed
- Use explicit loop variables instead of default `item`
- Use `true`/`false` not `yes`/`no` for boolean values
- No trailing whitespace in any files
- Ensure all playbooks work on Debian-based distributions
- Use YAML dictionaries for module arguments rather than inline key=value format
- Store secrets in vault-encrypted files with `.secret` extension
- Indent with 2 spaces for all YAML files