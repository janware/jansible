# Jansible

Welcome to Jansible! This repository contains a collection of Ansible playbooks for setting up systems, primarily for my personal HomeLab. Jansible is a combination of my name, Jan, and the configuration tool Ansible.

## Repository Structure

- `roles/`: Contains various Ansible roles for different configurations
  - `server/`: Tasks specific to the 'server' Proxmox server
  - `proxmox/`: Proxmox-related configurations
  - `software/`: Software installation and configuration
  - `users/`: User management tasks
  - `system/`: System-wide configurations
  - `reboot/`: Handles system reboots
- `inventory/host_vars/`: Host-specific variables
- `setup/`: Contains files for the GitLab page

## Playbooks

- common playbooks
  - `init.yml`: Initial setup for all hosts
  - `base_config.yml`: Base configuration for all hosts

- server playbooks
  - `server.yml`: Configuration for the Proxmox server
  - `srvdns2.yml`: Configuration for the Raspberry Pi (runs Pi-hole)
  - `srvdns1.yml`: Configuration for the PiHole LXC container
  - `lapjan01.yml`: Configuration for Jan's work laptop
  - `lapjan02.yml`: Configuration for Jan's personal laptop

## Setting up Git Hooks

To use the project's Git hooks:

1. Navigate to the repository root.
2. Run the following commands:

    `bash hooks/setup.sh`

This will copy the hooks to your local .git/hooks directory and make them executable.

## Key Features

- ZFS configuration for Proxmox
- Proxmox post-installation tasks
- Custom storage and pool configurations
- Automated system updates
- SSH hardening
- User management with sudo permissions
- System-wide configurations (clock, locale, logging, memory management, etc.)
- Monitoring tools setup (Node Exporter, Fluentd, Logstash, Kibana)

## Setup Instructions

To set up a new machine using Jansible, run one of the following commands:

### Using `wget`

```sh
wget -O - https://jansible.papasmurf.nl/setup.sh | sudo bash
```

### Using `curl`

```sh
curl https://jansible.papasmurf.nl/setup.sh | sudo bash
```

## Usage

1. Choose the appropriate playbook for your host.
2. Run the setup script as shown above.
3. The script will clone this repository and run the Ansible playbook for your host.

## Conventions

Please refer to the [CONVENTIONS.md](CONVENTIONS.md) file for coding and naming conventions used in this project.

## Gitlab CI/CD

The repository uses GitLab CI/CD to set up a GitLab page. The pipeline configuration can be found in the `.gitlab-ci.yml` file.

## GitLab Page

Access the GitLab page at https://jansible.papasmurf.nl, which redirects to the GitLab-hosted page. This page contains the setup script and other necessary files for the initial setup process.

## Contributing

If you'd like to contribute to Jansible, please fork the repository and create a pull request with your changes.

Thank you for your interest in Jansible!
