- name: system | zfs | get list of zfs pools
  command: zfs list -o name
  register: zfs_pools
  changed_when: false

- name: system | zfs | create zfs raid1
  command: zpool create {{ item.pool }} mirror {{ item.disk1 }} {{ item.disk2 }}
  when: item.pool not in zfs_pools.stdout_lines
  loop:
    - { pool: 'nvme_pool', disk1: '/dev/nvme0n1', disk2: '/dev/nvme1n1' }
    - { pool: 'media_pool', disk1: '/dev/sda', disk2: '/dev/sde' }
    - { pool: 'ssd_pool', disk1: '/dev/sdd', disk2: '/dev/sdf' }
  changed_when: false

- name: system | zfs | create zfs raid0
  command: zpool create {{ item.pool }} {{ item.disk }}
  when: item.pool not in zfs_pools.stdout_lines
  loop:
    - { pool: 'backup_pool', disk: '/dev/sdb' }
  changed_when: false

- name: system | zfs | create zfs datasets
  command: zfs create -o mountpoint={{ item.mnt }} {{ item.dataset }}
  when: item.dataset not in zfs_pools.stdout_lines
  loop:
    - { dataset: 'nvme_pool/vms', mnt: '/vms' }
    - { dataset: 'nvme_pool/cts', mnt: '/cts' }
    - { dataset: 'backup_pool/backups', mnt: '/backups' }
    - { dataset: 'backup_pool/iso', mnt: '/iso' }
    - { dataset: 'backup_pool/templates', mnt: '/templates' }
    - { dataset: 'media_pool/media', mnt: '/media' }
    - { dataset: 'ssd_pool/seafile', mnt: '/seafile' }
    - { dataset: 'ssd_pool/databases', mnt: '/databases' }
    - { dataset: 'ssd_pool/lxcdata', mnt: '/lxcdata' }
  changed_when: false

- name: system | zfs | check current dataset properties
  command: zfs get -H -o value {{ item.property }} {{ item.dataset }}
  register: current_properties
  loop:
    - { dataset: 'nvme_pool/vms', property: 'compression', value: 'lz4' }
    - { dataset: 'nvme_pool/vms', property: 'atime', value: 'off' }
    - { dataset: 'nvme_pool/vms', property: 'recordsize', value: '16K' }
    - { dataset: 'nvme_pool/cts', property: 'compression', value: 'lz4' }
    - { dataset: 'nvme_pool/cts', property: 'atime', value: 'off' }
    - { dataset: 'nvme_pool/cts', property: 'recordsize', value: '16K' }
    - { dataset: 'backup_pool/backups', property: 'compression', value: 'lz4' }
    - { dataset: 'backup_pool/backups', property: 'atime', value: 'off' }
    - { dataset: 'backup_pool/backups', property: 'recordsize', value: '1M' }
    - { dataset: 'backup_pool/iso', property: 'compression', value: 'lz4' }
    - { dataset: 'backup_pool/iso', property: 'atime', value: 'off' }
    - { dataset: 'backup_pool/iso', property: 'recordsize', value: '1M' }
    - { dataset: 'backup_pool/templates', property: 'compression', value: 'lz4' }
    - { dataset: 'backup_pool/templates', property: 'atime', value: 'off' }
    - { dataset: 'backup_pool/templates', property: 'recordsize', value: '1M' }
    - { dataset: 'media_pool/media', property: 'compression', value: 'off' }
    - { dataset: 'media_pool/media', property: 'atime', value: 'off' }
    - { dataset: 'media_pool/media', property: 'recordsize', value: '1M' }
    - { dataset: 'ssd_pool/seafile', property: 'compression', value: 'lz4' }
    - { dataset: 'ssd_pool/seafile', property: 'atime', value: 'on' }
    - { dataset: 'ssd_pool/seafile', property: 'recordsize', value: '128K' }
    - { dataset: 'ssd_pool/databases', property: 'compression', value: 'lz4' }
    - { dataset: 'ssd_pool/databases', property: 'atime', value: 'off' }
    - { dataset: 'ssd_pool/databases', property: 'recordsize', value: '8K' }
    - { dataset: 'ssd_pool/lxcdata', property: 'compression', value: 'lz4' }
    - { dataset: 'ssd_pool/lxcdata', property: 'atime', value: 'off' }
    - { dataset: 'ssd_pool/lxcdata', property: 'recordsize', value: '16K' }
    - { dataset: 'ssd_pool/lxcdata', property: 'acltype', value: 'posixacl' }
    - { dataset: 'ssd_pool/lxcdata', property: 'aclinherit', value: 'passthrough' }
    - { dataset: 'ssd_pool/lxcdata', property: 'aclmode', value: 'passthrough' }
  changed_when: false

- name: system | zfs | set dataset properties
  command: zfs set {{ item.item.property }}={{ item.item.value }} {{ item.item.dataset }}
  when: item.stdout != item.item.value
  loop: "{{ current_properties.results }}"
  changed_when: false

- name: system | zfs | set global properties
  command: zfs set {{ item.property }}={{ item.value }} {{ item.pool }}
  loop:
    - { pool: 'nvme_pool', property: 'atime', value: 'off' }
    - { pool: 'backup_pool', property: 'atime', value: 'off' }
    - { pool: 'media_pool', property: 'atime', value: 'off' }
    - { pool: 'ssd_pool', property: 'atime', value: 'off' }
    - { pool: 'nvme_pool', property: 'xattr', value: 'sa' }
    - { pool: 'backup_pool', property: 'xattr', value: 'sa' }
    - { pool: 'media_pool', property: 'xattr', value: 'sa' }
    - { pool: 'ssd_pool', property: 'xattr', value: 'sa' }
  changed_when: false
