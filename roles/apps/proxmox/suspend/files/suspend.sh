#!/bin/sh
case "$1" in
start)
   qm list | awk -F'[^0-9]*' '$0=$2' | while read -r vm_id; do qm unlock $vm_id; done;
;;
stop)
   qm list | grep running | awk -F'[^0-9]*' '$0=$2' | while read -r vm_id; do qm suspend $vm_id --todisk 1; done;
   sleep 10
;;
esac
exit 0
