---
- name: users | software | install software we need in this role
  package:
    state: present
    name:
      - stow
      - zsh
      - cron

- name: users | groups | docker and kvm
  group:
    name: "{{ item }}"
    state: present
  loop:
    - docker
    - libvirt

- name: users | groups | create for each user
  group:
    name: "{{ item.name }}"
    state: present
  loop: "{{ all_users }}"

- name: reset connection to apply changes
  meta: reset_connection

- name: users | accounts | create and configure
  user:
    name: "{{ item.name }}"
    groups: "{{ item.name }},docker,libvirt,sudo"
    shell: "{{ item.shell }}"
    uid: "{{ item.uid }}"
    system: false
    create_home: true
    home: "/home/{{ item.name }}"
    password: "{{ lookup('vars', item.password_var) }}"
  loop: "{{ all_users }}"

- name: users | home | set correct permissions
  file:
    path: "/home/{{ item.name }}"
    owner: "{{ item.name }}"
    group: "{{ item.name }}"
    mode: '0751'
  loop: "{{ all_users }}"

- name: users | cron | create crontabs directory
  file:
    path: "/var/spool/cron/crontabs"
    state: directory
    mode: '0755'
    owner: root
    group: root

- name: users | cron | create crontabs file for each user
  copy:
    content: ""
    dest: "/var/spool/cron/crontabs/{{ item.name }}"
    force: false
    mode: '0600'
    owner: "{{ item.name }}"
    group: crontab
  loop: "{{ all_users }}"

- name: users | sudo | set up permissions
  copy:
    src: "{{ item.sudoers_file }}"
    dest: "/etc/sudoers.d/{{ item.name }}"
    owner: root
    group: root
    mode: '0440'
  loop: "{{ all_users }}"

- name: users | dotfiles | create and update dotfiles for each user
  block:
    - name: users | dotfiles | ensure .dotfiles directory exists
      file:
        path: "/home/{{ item.name }}/.dotfiles"
        state: directory
        owner: "{{ item.name }}"
        group: "{{ item.name }}"
        mode: '0755'
      loop: "{{ all_users }}"

    - name: users | dotfiles | clone or update repository
      git:
        repo: 'https://oauth2:{{ gitlab_token }}@gitlab.com/opajan/dotfiles.git'
        dest: "/home/{{ item.name }}/.dotfiles"
        version: main
        update: true
        force: true
      become: true
      become_user: "{{ item.name }}"
      loop: "{{ all_users }}"
      register: git_result
      changed_when: git_result.changed and not git_result.before == git_result.after

    - name: users | dotfiles | run update script for each user
      command:
        cmd: "/home/{{ item.name }}/.update.sh -f"
        creates: "/home/{{ item.name }}/.dotfiles-initialized"
      become: true
      become_user: "{{ item.name }}"
      loop: "{{ all_users }}"
      register: update_result
      failed_when: false  # Allow the script to fail without stopping the playbook

    - name: users | dotfiles | mark user as initialized
      file:
        path: "/home/{{ item.name }}/.dotfiles-initialized"
        state: touch
        owner: "{{ item.name }}"
        group: "{{ item.name }}"
        mode: '0755'
      loop: "{{ all_users }}"
      when: update_result.changed

- name: users | dotfiles | deploy update script
  template:
    src: ".update.sh.j2"
    dest: "/home/{{ item.name }}/.update.sh"
    owner: "{{ item.name }}"
    group: "{{ item.name }}"
    mode: '0755'
  loop: "{{ all_users }}"

- name: users | dotfiles | deploy provision script
  copy:
    src: ".provision.sh"
    dest: "/home/{{ item.name }}/.provision.sh"
    owner: "{{ item.name }}"
    group: "{{ item.name }}"
    mode: '0755'
  loop: "{{ all_users }}"

- name: users | dotfiles | set up cron job for periodic updates
  cron:
    name: "Check and update dotfiles"
    minute: "*/15"
    job: "/home/{{ item.name }}/.update.sh"
    user: "{{ item.name }}"
  loop: "{{ all_users }}"

- name: set proper owner for oh-my-zsh
  file:
    path: "/home/{{ item.name }}/.oh-my-zsh"
    owner: "{{ item.name }}"
    group: "{{ item.name }}"
    mode: '0751'
    state: directory
  loop: "{{ all_users }}"

- name: install oh-my-zsh
  git:
    repo: 'https://github.com/robbyrussell/oh-my-zsh'
    dest: "/home/{{ item.name }}/.oh-my-zsh"
    version: master
    update: true
  become: true
  become_user: "{{ item.name }}"
  loop: "{{ all_users }}"
  register: ohmyzsh_result
  changed_when: ohmyzsh_result.changed and not ohmyzsh_result.before == ohmyzsh_result.after

- name: users | cron | clean up Downloads directory
  cron:
    name: "Clean up Downloads directory"
    minute: "0"
    hour: "*/6"  # Run every 6 hours
    job: "find /home/{{ item.name }}/Downloads -type f -atime +3 -delete && find /home/{{ item.name }}/Downloads -type d -empty -delete"
    user: "{{ item.name }}"
  loop: "{{ all_users }}"

# Enhance security by locking the root account password
# unless defined otherwise in the host_vars folder
- name: users | root | manage account password lock
  user:
    name: root
    password_lock: "{{ lock_root | default(true) }}"

