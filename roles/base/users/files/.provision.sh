#!/bin/bash

# Run provision script as user johnny, regardless of who executes this script
if [ "$1" = "-f" ] || [ "$1" = "--force" ]; then
    sudo -u johnny sudo /usr/local/bin/provision -f
else
    sudo -u johnny sudo /usr/local/bin/provision
fi
