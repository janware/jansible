---
- name: software | utilities | set common package list
  set_fact:
    common_packages:
      - at
      - colordiff
      - curl
      - "{{ 'exa' if 'proxmox_hosts' in group_names else 'eza' }}"
      - fuse
      - fzf
      - htop
      - iotop
      - iperf
      - jq
      - kitty
      - kitty-terminfo
      - lftp
      - lm-sensors
      - mc
      - net-tools
      - nmap
      - parted
      - ripgrep
      - rsync
      - tmux
      - traceroute
      - tree
      - unzip
      - usbutils
      - vim
      - wget
      - whois
      - zsh
      - dnsutils
      - lsb-release
      - iw
      - wireless-tools
      - net-tools
      - rfkill

- name: software | utilities | add non-amd64 specific packages
  set_fact:
    common_packages: "{{ common_packages + ['docker', 'docker-compose'] }}"
  when: "'docker_hosts' in group_names"

- name: software | utilities | add Debian-specific packages
  set_fact:
    common_packages: "{{ common_packages + ['telnet', 'nala'] }}"

- name: software | utilities | install common system packages
  package:
    state: present
    name: "{{ common_packages }}"

- name: software | utilities | create eza symlink on Proxmox hosts
  file:
    src: /usr/bin/exa
    dest: /usr/bin/eza
    state: link
  when: "'proxmox_hosts' in group_names"

- name: software | docker | enable and start
  service:
    name: docker
    enabled: true
    state: started
  become: true
  when: "'docker_hosts' in group_names"

- name: software | utilities | install Neovim on amd64 hosts
  package:
    name: neovim
    state: present
  when: "'amd64_hosts' in group_names"

- name: software | utilities | install Neovim on non-amd64 hosts
  block:
    - name: software | utilities | create nvim directory
      file:
        path: /opt/nvim
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: software | utilities | download nvim AppImage
      get_url:
        url: 'https://github.com/neovim/neovim/releases/latest/download/nvim-linux-x86_64.appimage'
        dest: /opt/nvim/nvim.appimage
        mode: '0755'
        owner: root
        group: root

    - name: software | utilities | extract nvim AppImage
      command: 
        cmd: "/opt/nvim/nvim.appimage --appimage-extract"
        chdir: /opt/nvim
        creates: /opt/nvim/squashfs-root

    - name: software | utilities | create nvim symlink
      file:
        src: /opt/nvim/squashfs-root/usr/bin/nvim
        dest: /usr/local/bin/nvim
        state: link
        force: yes
  when: "'amd64_hosts' not in group_names"

- name: software | utilities | check croc installation status
  command:
    cmd: "which croc"
  register: check_croc
  changed_when: false
  failed_when: false

- name: software | utilities | install croc file transfer tool
  when: check_croc.rc != 0
  block:
    - name: software | utilities | download croc installer
      get_url:
        url: https://getcroc.schollz.com
        dest: /tmp/install_croc.sh
        mode: '0755'
    - name: software | utilities | run croc installer
      command:
        cmd: "/tmp/install_croc.sh"
      changed_when: true
    - name: software | utilities | remove croc installer
      file:
        path: /tmp/install_croc.sh
        state: absent
