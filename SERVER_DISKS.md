

# Proxmox Server Storage Documentation

## Physical Disk Configuration

### System Overview
The server is equipped with 8 physical storage devices:

#### NVMe Drives
- 2x Samsung SSD 970 EVO Plus 1TB (nvme0n1, nvme1n1)
  - High-performance NVMe SSDs
  - Each drive: 931.5GB usable capacity
  - Configured in mirror for nvme_pool

#### SATA SSDs
- 1x Samsung SSD 870 EVO 250GB (sdc)
  - 232.9GB usable capacity
  - Used for root pool (rpool)

- 1x Samsung SSD 870 QVO 4TB (sdd)
  - 3.6TB usable capacity
  - Part of mirrored ssd_pool

- 1x Crucial MX500 4TB (sdf)
  - 3.6TB usable capacity
  - Part of mirrored ssd_pool

#### Hard Drives
- 1x Seagate ST2000VN004 2TB (sda)
  - 1.8TB usable capacity
  - Part of mirrored media_pool

- 1x Seagate ST1000LM024 1TB (sdb)
  - 931.5GB usable capacity
  - Single disk in backup_pool

- 1x Seagate ST2000LM007 2TB (sde)
  - 1.8TB usable capacity
  - Part of mirrored media_pool

## ZFS Pool Configuration

### ZFS Dataset Structure

1. **backup_pool**
   - /backup_pool/backups
   - /backup_pool/iso
   - /backup_pool/templates

2. **media_pool**
   - /media_pool/media

3. **nvme_pool**
   - /nvme_pool/cts
   - /nvme_pool/vms

4. **rpool**
   - /rpool/ROOT
   - /rpool/ROOT/pve-1 (mounted as /)
   - /rpool/data
   - /rpool/var-lib-vz

5. **ssd_pool**
   - /ssd_pool/databases
   - /ssd_pool/lxcdata (ACLs enabled for shared data access)
   - /ssd_pool/seafile

### Pool Organization
1. **nvme_pool** (High Performance)
   - Configuration: Mirror of two NVMe drives
   - Purpose: Virtual machines and containers
   - Size: 928GB

2. **ssd_pool** (Fast Storage)
   - Configuration: Mirror of two 4TB SSDs (sdd + sdf)
   - Purpose: Database and container data
   - Size: 3.62TB

3. **media_pool** (Large Storage)
   - Configuration: Mirror of two 2TB HDDs (sda + sde)
   - Purpose: Media storage
   - Size: 1.81TB

4. **backup_pool** (Backup Storage)
   - Configuration: Single disk (sdb)
   - Purpose: Backups and ISO storage
   - Size: 928GB

5. **rpool** (System Pool)
   - Configuration: Single SSD partition (sdc-part3)
   - Purpose: Root filesystem and system data
   - Size: 230GB

## Proxmox Storage Configuration

### ZFS Pool Storage
- **vms** (zfspool)
  - Mapped to: nvme_pool/vms
  - Implementation: ZFS Volumes (zvols)
  - Purpose: VM disk images

- **cts** (zfspool)
  - Mapped to: nvme_pool/cts
  - Implementation: ZFS Datasets
  - Purpose: Container storage

### Directory Storage
- **backups** (dir)
  - Mapped to: /backups (backup_pool/backups)
  - Purpose: VM and CT backups

- **iso** (dir)
  - Mapped to: /iso (backup_pool/iso)
  - Purpose: ISO images and cloud-init images

- **templates** (dir)
  - Mapped to: /templates (backup_pool/templates)
  - Purpose: Container templates

- **local** (dir)
  - Mapped to: Local system directory
  - Purpose: Local system storage

- **local-zfs** (zfspool)
  - Mapped to: rpool
  - Purpose: Local system storage

## Redundancy Configuration
- All critical pools are mirrored (RAID1):
  - nvme_pool: 2x NVMe in mirror
  - ssd_pool: 2x 4TB SSDs in mirror
  - media_pool: 2x 2TB HDDs in mirror
- Non-redundant pools:
  - backup_pool: Single disk
  - rpool: Single SSD partition

## Storage Totals

### Physical Storage
- NVMe Storage: 1.82TB (2x 931.5GB)
- SSD Storage: 7.43TB (1x 232.9GB + 2x 3.6TB)
- HDD Storage: 4.53TB (2x 1.8TB + 1x 931.5GB)
**Total Raw Storage: 13.78TB**

### ZFS Pool Storage
- nvme_pool: 928GB (mirrored from 1.82TB raw)
- ssd_pool: 3.62TB (mirrored from 7.2TB raw)
- media_pool: 1.81TB (mirrored from 3.6TB raw)
- backup_pool: 928GB (single disk)
- rpool: 230GB (single partition)
**Total Usable Pool Storage: 7.52TB**

## Performance Tier Structure
1. Tier 1 (Fastest): nvme_pool - NVMe drives in mirror
2. Tier 2 (Fast): ssd_pool - SATA SSDs in mirror
3. Tier 3 (Bulk): media_pool - HDDs in mirror
4. Tier 4 (Backup): backup_pool - Single HDD
5. Tier 5 (System): rpool - SSD partition
