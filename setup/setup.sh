#!/bin/bash

# Function to clean up before exiting
cleanup() {
    stty sane
    exit 1
}

# Trap Ctrl+C signal to run the cleanup function
trap cleanup SIGINT

# Check if running as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    cleanup
fi

apt update
apt install -y util-linux

# Synchronize system clock with the date header from Google's HTTP response
date -s "$(curl -s --head http://google.com | grep ^Date: | sed 's/Date: //g')" || {
    echo "Failed to synchronize system clock."
}

echo -n "Enter vault password: "
read -rs password1 < /dev/tty
echo

# Check if password is empty
if [[ -z "$password1" ]]; then
    echo "No password provided. Exiting."
    cleanup
fi

# Prompt to repeat the password for verification
echo -n "Repeat password for verification: "
read -rs password2 < /dev/tty
echo

# Check if both passwords match
if [ "$password1" == "$password2" ]; then
    echo "$password1" > /etc/.vault.txt
else
    echo "Sorry, aborting... passwords don't match."
    cleanup
fi

# Install dependencies
if [ -x "$(command -v apt)" ]; then 
    apt update
    apt install -y python3 python3-pip git ansible sudo
else
    echo "Unsupported package manager. Please install the required packages manually."
    cleanup
fi

# Ensure ansible commands are available in PATH
export PATH=$PATH:/usr/bin

# Install Ansible Galaxy collection for community modules
ansible-galaxy install stefangweichinger.ansible_rclone
ansible-galaxy collection install community.general
ansible-galaxy collection install kewlfft.aur
ansible-galaxy collection install ansible.posix
ansible-galaxy collection install community.docker

# Create and set permissions for ansible.log
touch /var/log/ansible.log
chmod 777 /var/log/ansible.log

# Set the destination directory for ansible-pull
DEST_DIR="$HOME/ansible-pull"

# Run ansible-pull with a playbook that 
#   - initializes the ansible user 
#   - schedules the ansible job for further and future configuration
REPO_URL="https://gitlab.com/opajan/jansible.git"
ansible-pull -v -U "$REPO_URL" -c local playbooks/bootstrap/init.yml
