# server.yml Documentation

## Overview

server.yml is an Ansible playbook designed to configure the server Proxmox server. It sets up various aspects of the server, including storage, system configuration, and software installation.

## Key Components

1. **Pre-tasks**
   - Runs post-installation tasks for Proxmox
   - Updates the package cache

2. **Roles**
   - server/tools: Installs necessary tools for Proxmox
   - server/zfs_config: Configures ZFS storage
   - server/storage: Sets up storage for Proxmox
   - server/proxmox_pools: Configures Proxmox pools
   - proxmox/suspend: Handles suspension tasks for Proxmox
   - users: Manages user accounts
   - system: Configures system-wide settings
   - software: Installs and configures software packages

3. **Variables**
   - storage_configs: Defines various storage configurations
   - pool_configs: Specifies Proxmox pool configurations

## Usage

To run this playbook:

```
ansible-playbook server.yml
```

## Notes

- This playbook is specifically tailored for the server Proxmox server.
- Ensure that the group_vars `all.yml` file is properly configured with necessary variables.
- Some tasks that are commented out in the original playbook (e.g., oh_my_zsh, neovim, fastfetch) are not included in the current implementation.

## Future Improvements

Consider implementing the commented-out tasks for a more comprehensive server setup:
- oh_my_zsh installation
- neovim configuration
- fastfetch setup
- Additional system setup tasks (e.g., grub configuration)
